import { baseURL } from "../config.json";

export async function getDevices() {
  return await fetch(`${baseURL}/api/v1/device`).then((res) => {
    if (res.ok) return res.json();
    return res;
  });
}

export async function getMeasurement(deviceID, duration) {
  const to = new Date();
  const from = new Date(to.valueOf());
  from.setHours(from.getHours() - duration);

  return await fetch(
    `${baseURL}/api/v1/device/${deviceID}/measurement?to=${to.toISOString()}&from=${from.toISOString()}`
  ).then((res) => {
    if (res.ok) return res.json();
    return res;
  });
}

export async function getMetric(deviceID, duration) {
  const to = new Date();
  const from = new Date(to.valueOf());
  from.setHours(from.getHours() - duration);

  return await fetch(
    `${baseURL}/api/v1/device/${deviceID}/metric?to=${to.toISOString()}&from=${from.toISOString()}`
  ).then((res) => {
    if (res.ok) return res.json();
    return res;
  });
}
