import PowerInputIcon from "@mui/icons-material/PowerInput";
import BoltIcon from "@mui/icons-material/Bolt";
import TungstenIcon from "@mui/icons-material/Tungsten";
import { Box, Grid, Typography } from "@mui/material";
import Item from "../../themic/Item";
import "./index.css";

function MeasurementStat({ powerMw, currentMa, voltageMv }) {
  return (
    <div className="stat__measurement">
      <Box>
        <Grid container spacing={3}>
          <Grid item xs={4}>
            <Item className="stat__attribute">
              <PowerInputIcon />
              <br />
              <Typography variant="p" component="p">
                {powerMw.mean / 1000}W
              </Typography>
            </Item>
          </Grid>
          <Grid item xs={4}>
            <Item className="stat__attribute">
              <BoltIcon />
              <br />
              <Typography variant="p" component="p">
                {voltageMv.mean / 1000}V
              </Typography>
            </Item>
          </Grid>
          <Grid item xs={4}>
            <Item className="stat__attribute">
              <TungstenIcon />
              <br />
              <Typography variant="p" component="p">
                {currentMa.mean / 1000}A
              </Typography>
            </Item>
          </Grid>
        </Grid>
      </Box>
    </div>
  );
}

export default MeasurementStat;
