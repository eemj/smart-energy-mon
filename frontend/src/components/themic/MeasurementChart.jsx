import {
  CartesianGrid,
  Legend,
  Line,
  LineChart,
  ReferenceLine,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";

function MeasurementChart({
  measurements,
  skipSeconds,
  stats: { maximum, minimum, mean },
}) {
  const convert = (value) => value / 1_000;

  const data = measurements
    .map((measurement) => ({
      ...measurement,
      date: new Date(measurement.timestamp),
      powerW: convert(measurement.powerMw),
    }))
    .filter(({ date, powerMw }) => {
      const hourSeconds = date.getHours() * 60 * 60 + date.getSeconds();
      return (
        hourSeconds % skipSeconds === 0 ||

        // We can't skip the maximum/min/mean to show the reference lines.
        powerMw === mean ||
        powerMw === maximum ||
        powerMw === minimum
      );
    });

  return (
    <ResponsiveContainer
      width="100%"
      height={Math.max(window.innerHeight - 300, 500)}
    >
      <LineChart data={data}>
        <ReferenceLine
          y={convert(maximum)}
          label="Minimum"
          stroke="red"
        />
        <ReferenceLine
          y={convert(minimum)}
          label="Maximum"
          stroke="yellow"
        />
        <ReferenceLine
          y={convert(mean)}
          label="Mean"
          stroke="orange"
        />
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey={"timestamp"} />
        <YAxis
          type="number"
          tickCount={skipSeconds}
          padding={{ left: 30, right: 30 }}
        />
        <Tooltip cursor={false} />
        <Legend height={36} verticalAlign={"top"} />
        <Line
          dot={false}
          name="Power in Watts / Second"
          type={"linear"}
          dataKey={"powerW"}
          stroke="#42a5f5"
        />
      </LineChart>
    </ResponsiveContainer>
  );
}

export default MeasurementChart;
