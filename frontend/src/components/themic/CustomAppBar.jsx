import AppBar from "@mui/material/AppBar";
import Box from "@mui/material/Box";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import DeviceHubIcon from "@mui/icons-material/DeviceHub";
import TimelapseIcon from "@mui/icons-material/Timelapse";
import LeaderboardIcon from "@mui/icons-material/Leaderboard";
import { Avatar, Button, Chip, MenuItem, Select } from "@mui/material";
import { NavLink, useNavigate } from "react-router-dom";

import { durations } from "../../config.json";

export default function CustomAppBar({
  deviceCount,
  duration,
  onDurationChange,
}) {
  const handleDurationChange = (event) => {
    const eventValue = Number(event.target.value);
    onDurationChange(durations.find(({ value }) => value === eventValue));
  };
  
  let navigate = useNavigate();

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="static">
        <Toolbar>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }} onClick={() => {
            navigate('/');
          }}>
            Smart Energy Monitoring
          </Typography>
          
          <NavLink to={'/leaderboard'}>
            <Button
              variant="text"
              startIcon={<LeaderboardIcon />}
              aria-label="Leaderboard"
              title="Leaderboard"
              sx={{ marignLeft: 1, marginRight: 1, textTransform: 'underline' }}
            >
              Leaderboard
            </Button>
          </NavLink>
          <TimelapseIcon
            color="primary"
            sx={{ marginLeft: 1, marginRight: 1 }}
          />
          <Select
            color="primary"
            variant="standard"
            sx={{ marginRight: 3 }}
            value={duration?.value}
            aria-label="Duration"
            label="Duration"
            title="Duration"
            onChange={handleDurationChange}
          >
            {durations.map(({ value, name }) => (
              <MenuItem key={value} value={value}>
                {name}
              </MenuItem>
            ))}
          </Select>
          <Chip
            color="primary"
            icon={<DeviceHubIcon />}
            label={deviceCount ?? 0}
            sx={{ marginRight: 3 }}
          />
          <Avatar alt="John Doe">G</Avatar>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
