import { Container } from "@mui/material";
import { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import { durations } from "./config.json";

import CustomAppBar from "./components/themic/CustomAppBar";
import AppBarContext from "./context/AppBarContext";

import "./App.css";
import { getDevices } from "./adapter/adapter";

function App() {
  const [duration, setDuration] = useState(durations[0]);
  const [devices, setDevices] = useState([]);

  useEffect(() => {
    getDevices().then(({ data }) => setDevices(data));
  }, []);

  return (
    <div className="App">
      <AppBarContext.Provider
        value={{
          duration,
          devices,
        }}
      >
        <CustomAppBar
          duration={duration}
          deviceCount={devices?.length ?? 0}
          onDurationChange={setDuration}
        />

        <Container className="main">
          <Outlet />
        </Container>
      </AppBarContext.Provider>
    </div>
  );
}

export default App;
