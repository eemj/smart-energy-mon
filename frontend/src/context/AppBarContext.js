import React from "react";
import { durations } from "../config.json";

const AppBarContext = React.createContext({
  duration: durations[0],
  devices: [],
});

export default AppBarContext;