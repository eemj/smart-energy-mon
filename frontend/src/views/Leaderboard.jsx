import {
  Paper,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableHead,
  TableRow,
  Typography,
} from "@mui/material";
import { useContext, useEffect, useState } from "react";
import AppBarContext from "../context/AppBarContext";
import { getMetric } from "../adapter/adapter";
import { ArrowDownward, ArrowUpward } from "@mui/icons-material";
import "../styles/leaderboard.css";

const compareStat = (previous, current, flip = false) => {
  const stat = {
    value: current / 1_000,
    difference: Math.abs(previous - current) / 1_000,
  };

  if (flip) {
    stat.wasLess = previous > current;
    stat.icon = previous > current ? ArrowDownward : ArrowUpward;
    stat.sign = previous > current ? '-' : '+'
  } else {
    stat.wasLess = previous < current;
    stat.icon = previous < current ? ArrowUpward : ArrowDownward;
    stat.sign = previous < current ? '-' : '+'
  }

  return stat;
};

function Leaderboard() {
  const { duration, devices } = useContext(AppBarContext);
  const [metrics, setMetrics] = useState([]);

  const getAllMetrics = () =>
    Promise.all(devices.map(({ id }) => getMetric(id, duration.value))).then(
      (responses) => {
        setMetrics(
          responses.map(({ data: { device, stats, previousStats } }) => ({
            device,
            compare: {
              powerMw: {
                maximum: compareStat(
                  previousStats.powerMw.maximum,
                  stats.powerMw.maximum
                ),
                minimum: compareStat(
                  previousStats.powerMw.minimum,
                  stats.powerMw.minimum,
                ),
                mean: compareStat(
                  previousStats.powerMw.mean,
                  stats.powerMw.mean,
                  true,
                ),
                standardDeviation: compareStat(
                  previousStats.powerMw.standardDeviation,
                  stats.powerMw.standardDeviation,
                  true,
                ),
              },
            },
          }))
        );
      }
    );

  useEffect(() => {
    const interval = window.setInterval(getAllMetrics, 5_000);

    getAllMetrics();

    return () => {
      window.clearInterval(interval);
    };
  }, [devices]);

  if (metrics.length === 0)
    return (
      <Typography variant="p" component="p">
        Loading
      </Typography>
    );

  return (
    <>
      <TableContainer component={Paper} sx={{ marginTop: 5 }}>
        <Table>
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>MAC Address</TableCell>
              <TableCell>Mean Power</TableCell>
              <TableCell>Maximum Power</TableCell>
              <TableCell>Minimum Power</TableCell>
              <TableCell>Power Deviation</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {metrics.map(({ device, compare: { powerMw: compare } }) => {
              return (
                <TableRow key={device.id}>
                  <TableCell>{device.name}</TableCell>
                  <TableCell>{device.macAddress}</TableCell>
                  <TableCell
                    className={`compare__${
                      !compare.mean.wasLess ? "green" : "red"
                    }`}
                  >
                    <div className="cell-value">
                      <compare.mean.icon fontSize="small" />
                      &nbsp;
                      {compare.mean.sign}
                      {compare.mean.value}W
                    </div>
                  </TableCell>
                  <TableCell
                    className={`compare__${
                      !compare.maximum.wasLess ? "red" : "green"
                    }`}
                  >
                    <div className="cell-value">
                      <compare.maximum.icon />
                      &nbsp;
                      {compare.maximum.value}W
                      ({compare.maximum.sign}{compare.maximum.difference}W)
                    </div>
                  </TableCell>
                  <TableCell
                    className={`compare__${
                      !compare.minimum.wasLess ? "red" : "green"
                    }`}
                  >
                    <div className="cell-value">
                      <compare.minimum.icon />
                      &nbsp;
                      {compare.minimum.value}W
                      ({compare.minimum.sign}{compare.minimum.difference}W)
                    </div>
                  </TableCell>
                  <TableCell
                    className={`compare__${
                      !compare.standardDeviation.wasLess ? "green" : "red"
                    }`}
                  >
                    <div className="cell-value">
                      <compare.standardDeviation.icon />
                      &nbsp;
                      {compare.standardDeviation.sign}
                      {compare.standardDeviation.value.toFixed(4)}W
                    </div>
                  </TableCell>
                </TableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>
    </>
  );
}

export default Leaderboard;
