import { useContext, useState, useEffect } from "react";
import { Container, Box, Button } from "@mui/material";
import MeasurementStat from "../components/stats/MeasurementStat";
import MeasurementChart from "../components/themic/MeasurementChart";

import { getMeasurement } from "../adapter/adapter";
import AppBarContext from "../context/AppBarContext";

function Home() {
  const { duration, devices } = useContext(AppBarContext);

  const [deviceId, setDeviceId] = useState(null);
  const [measurement, setMeasurement] = useState({
    id: null,
    data: {},
  });

  useEffect(() => {
    if (deviceId === null) return;

    getMeasurement(deviceId, duration.value).then(({ data }) => {
      setMeasurement({ id: deviceId, data });
    });
  }, [deviceId]);

  return (
    <Container className="main__wrapper">
      <Box
        className="device__container"
        flex={true}
        justifyContent={"center"}
        alignContent={"center"}
      >
        {devices.map(({ id, name }) => (
          <Button
            key={id}
            variant={deviceId === id ? "contained" : "outlined"}
            onClick={() => setDeviceId(id)}
            aria-selected={deviceId == id}
            sx={{ marginRight: 2, marginLeft: 2, textTransform: "none" }}
          >
            {name}
          </Button>
        ))}
      </Box>

      {measurement.id !== null && (
        <>
          <Box xs={{ width: "100%" }} sx={{ marginBottom: 2 }} className="measurement__container">
            <MeasurementStat {...measurement.data.stats} />
          </Box>
          <MeasurementChart
            stats={measurement.data.stats.powerMw}
            measurements={measurement.data.measurements}
            skipSeconds={duration.skipSeconds}
          />
        </>
      )}
    </Container>
  );
}

export default Home;
