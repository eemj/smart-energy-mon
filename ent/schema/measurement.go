package schema

import (
	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// Measurement holds the schema definition for the Measurement entity.
type Measurement struct {
	ent.Schema
}

// Fields of the Measurement.
func (Measurement) Fields() []ent.Field {
	return []ent.Field{
		field.Time("timestamp").Immutable().StructTag(`json:"timestamp"`),
		field.Int64("voltage_mv").NonNegative().Min(0).StructTag(`json:"voltageMv"`),
		field.Int64("current_ma").NonNegative().Min(0).StructTag(`json:"currentMa"`),
		field.Int64("power_mw").NonNegative().Min(0).StructTag(`json:"powerMw"`),
	}
}

// Indexes of the Measurement.
func (Measurement) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("timestamp"),
	}
}

// Edges of the Measurement.
func (Measurement) Edges() []ent.Edge {
	return []ent.Edge{
		edge.From("device", Device.Type).Ref("measurements").Unique(),
	}
}
