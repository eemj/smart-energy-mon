package schema

import (
	"regexp"

	"entgo.io/ent"
	"entgo.io/ent/schema/edge"
	"entgo.io/ent/schema/field"
	"entgo.io/ent/schema/index"
)

// Device holds the schema definition for the Device entity.
type Device struct {
	ent.Schema
}

// Fields of the Device.
func (Device) Fields() []ent.Field {
	return []ent.Field{
		field.String("mac_address").
			Immutable().
			NotEmpty().
			Match(regexp.MustCompile(`(([a-fA-F0-9]{2}[:]){5}[a-fA-F0-9]{2})`)).
			StructTag(`json:"macAddress"`),
		field.String("name").
			Unique().
			NotEmpty().
			StructTag(`json:"name"`),
	}
}

// Indexes of the Device.
func (Device) Indexes() []ent.Index {
	return []ent.Index{
		index.Fields("mac_address"),
	}
}

// Edges of the Device.
func (Device) Edges() []ent.Edge {
	return []ent.Edge{
		edge.To("measurements", Measurement.Type),
	}
}
