package httpsrv

import (
	"fmt"
	"net/http"
	"strconv"
	"time"

	"github.com/go-chi/chi/v5"
	"gitlab.com/eemj/smart-energy-mon/ent"
	"gitlab.com/eemj/smart-energy-mon/internal/mapper"
	"gitlab.com/eemj/smart-energy-mon/internal/repository/measurement"
	"gitlab.com/eemj/smart-energy-mon/internal/service/metric"
	"go.uber.org/zap"
)

func (srv *HTTPSrv) RegisterAPI() {
	srv.Route("/api/v1", func(router chi.Router) {
		router.Get("/device", func(w http.ResponseWriter, r *http.Request) {
			devices, err := srv.repo.Device().All(r.Context())

			if err != nil {
				respondErr(w, http.StatusInternalServerError, &ErrorDetails{Message: err.Error()})
				return
			}

			respondOk(w, http.StatusOK, mapper.MapDevicesEntityToContract(devices...))
		})

		router.Get("/device/{deviceID:[0-9]+}", func(w http.ResponseWriter, r *http.Request) {
			deviceID, _ := strconv.Atoi(chi.URLParam(r, "deviceID"))

			if deviceID <= 0 {
				respondErr(w, http.StatusBadRequest, &ErrorDetails{
					Key:     `validation_error`,
					Message: `'deviceID' must be greater than 0`,
				})
				return
			}

			device, err := srv.repo.Device().
				First(
					r.Context(),
					&ent.Device{ID: deviceID},
				)

			if err != nil {
				if ent.IsNotFound(err) {
					respondErr(w, http.StatusNotFound, &ErrorDetails{
						Key:     `device_not_found`,
						Message: fmt.Sprintf("'%d' is not found", deviceID),
					})
					return
				}

				respondErr(w, http.StatusInternalServerError, &ErrorDetails{Message: err.Error()})
				return
			}

			respondOk(w, http.StatusOK, mapper.MapDeviceEntityToContract(device))
		})

		router.Get("/device/{deviceID:[0-9]+}/measurement", func(w http.ResponseWriter, r *http.Request) {
			deviceID, _ := strconv.Atoi(chi.URLParam(r, "deviceID"))

			if deviceID <= 0 {
				respondErr(w, http.StatusBadRequest, &ErrorDetails{
					Key:     `validation_error`,
					Message: `'deviceID' must be greater than 0`,
				})
				return
			}

			filter := measurement.MeasurementFilter{
				ID: deviceID,
			}

			if _fromTime, err := time.Parse(time.RFC3339, r.URL.Query().Get("from")); err == nil && !_fromTime.IsZero() {
				filter.From = &_fromTime
			}

			if _toTime, err := time.Parse(time.RFC3339, r.URL.Query().Get("to")); err == nil && !_toTime.IsZero() {
				filter.To = &_toTime
			}

			measurements, err := srv.repo.Measurement().Find(r.Context(), filter)

			if err != nil {
				if ent.IsNotFound(err) {
					respondErr(w, http.StatusNotFound, &ErrorDetails{
						Key:     `measurement_not_found`,
						Message: fmt.Sprintf("measurement for '%d' not found", deviceID),
					})
					return
				}

				respondErr(w, http.StatusInternalServerError, &ErrorDetails{Message: err.Error()})
				return
			}

			mapped := mapper.MapMeasurementEntityToContract(measurements...)

			respondOk(w, http.StatusOK, mapped)
		})

		router.Get("/device/{deviceID:[0-9]+}/metric", func(w http.ResponseWriter, r *http.Request) {
			deviceID, _ := strconv.Atoi(chi.URLParam(r, "deviceID"))

			if deviceID <= 0 {
				respondErr(w, http.StatusBadRequest, &ErrorDetails{
					Key:     `validation_error`,
					Message: `'deviceID' must be greater than 0`,
				})
				return
			}

			filter := metric.GetMetricFilter{
				DeviceID: deviceID,
			}

			_fromTime, err := time.Parse(time.RFC3339, r.URL.Query().Get("from"))

			if err != nil {
				respondErr(w, http.StatusBadRequest, &ErrorDetails{
					Key:     `validation_error`,
					Message: err.Error(),
				})
				return
			}

			if !_fromTime.IsZero() {
				filter.RangeFrom = _fromTime
			}

			_toTime, err := time.Parse(time.RFC3339, r.URL.Query().Get("to"))

			if err != nil {
				respondErr(w, http.StatusBadRequest, &ErrorDetails{
					Key:     `validation_error`,
					Message: err.Error(),
				})
				return
			}

			zap.L().Info("filter", zap.String("from", _fromTime.Format(time.RFC3339)), zap.String("to", _toTime.Format(time.RFC3339)))

			if !_toTime.IsZero() {
				filter.RangeTo = _toTime
			}

			metrics, err := srv.metricService.Get(r.Context(), filter)

			if err != nil {
				if ent.IsNotFound(err) {
					respondErr(w, http.StatusNotFound, &ErrorDetails{
						Key:     `metric_not_found`,
						Message: fmt.Sprintf("measurement for '%d' not found", deviceID),
					})
					return
				}

				respondErr(w, http.StatusInternalServerError, &ErrorDetails{Message: err.Error()})
				return
			}

			respondOk(w, http.StatusOK, metrics)
		})
	})
}
