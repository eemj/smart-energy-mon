package httpsrv

import (
	"encoding/json"
	"errors"
	"net/http"
	"strings"

	"go.uber.org/zap"
)

type ErrorDetails struct {
	Key     string `json:"key"`
	Message string `json:"message"`
}

type ErrorResponse struct {
	Error *ErrorDetails `json:"error,omitempty"`
}

type OkResponse[V any] struct {
	Data V `json:"data,omitempty"`
}

func respondJSON(w http.ResponseWriter, statusCode int, v any) {
	w.Header().Add("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	json.NewEncoder(w).Encode(v)
}

func respondOk[V any](w http.ResponseWriter, statusCode int, data V) {
	respondJSON(w, statusCode, &OkResponse[V]{Data: data})
}

func respondErr(w http.ResponseWriter, statusCode int, errDetails *ErrorDetails) {
	if statusCode == http.StatusInternalServerError &&
		len(strings.TrimSpace(errDetails.Message)) > 0 {

		zap.L().WithOptions(
			zap.AddCaller(),
			zap.AddStacktrace(zap.ErrorLevel),
		).Error(
			"http",
			zap.Error(errors.New(errDetails.Message)),
		)

		respondJSON(w, statusCode, ErrorResponse{Error: &ErrorDetails{
			Key:     `something_went_wrong`,
			Message: `Something went wrong!`,
		}})

		return
	}

	if errDetails != nil {
		respondJSON(w, statusCode, ErrorResponse{Error: errDetails})
	} else {
		respondJSON(w, statusCode, ErrorResponse{Error: &ErrorDetails{
			Key:     "something_went_wrong",
			Message: "Something went wrong!",
		}})
	}
}
