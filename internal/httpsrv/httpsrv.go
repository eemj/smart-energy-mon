package httpsrv

import (
	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/go-chi/cors"
	"gitlab.com/eemj/smart-energy-mon/internal/repository"
	"gitlab.com/eemj/smart-energy-mon/internal/service/metric"
)

type HTTPSrv struct {
	*chi.Mux

	repo          repository.Repository
	metricService metric.MetricService
}

func New(repo repository.Repository) *HTTPSrv {
	httpSrv := &HTTPSrv{
		Mux:           chi.NewRouter(),
		repo:          repo,
		metricService: metric.NewMetric(repo),
	}

	httpSrv.Use(middleware.Logger)
	httpSrv.Use(middleware.Recoverer)
	httpSrv.Use(middleware.RealIP)
	httpSrv.Use(middleware.Heartbeat("/healthz"))
	httpSrv.Use(cors.AllowAll().Handler)

	httpSrv.RegisterAPI()

	return httpSrv
}
