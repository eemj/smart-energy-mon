package config

import (
	"fmt"

	"github.com/spf13/viper"
	"go.uber.org/zap"
)

type Database struct {
	Hostname string `mapstructure:"hostname" json:"hostname"`
	Port     int    `mapstructure:"port" json:"port"`
	Username string `mapstructure:"username" json:"username"`
	Password string `mapstructure:"password" json:"password"`
	Database string `mapstructure:"database" json:"database"`
	SSLMode  string `mapstructure:"sslmode" json:"sslMode"`
}

func (d Database) ConnectionString() string {
	return fmt.Sprintf(
		"host=%s port=%d user=%s dbname=%s password=%s sslmode=%s",
		d.Hostname,
		d.Port,
		d.Username,
		d.Database,
		d.Password,
		d.SSLMode,
	)
}

type Device struct {
	Username *string `mapstructure:"username,omitempty" json:"username,omitempty"`
	Password *string `mapstructure:"password,omitempty" json:"password,omitempty"`
	Address  string  `mapstructure:"address" json:"address"`
}

type Config struct {
	Devices  []Device `mapstructure:"devices" json:"devices"`
	Database Database `mapstructure:"database" json:"database"`
}

func New() (*Config, error) {
	v := viper.New()

	v.SetConfigName("config")
	v.SetConfigType("json")

	v.AddConfigPath("/etc/smg/")
	v.AddConfigPath(".")

	if err := v.ReadInConfig(); err != nil {
		zap.L().Fatal("config", zap.Error(err))
	}

	cfg := new(Config)

	if err := v.UnmarshalExact(cfg); err != nil {
		return nil, err
	}

	return cfg, nil
}
