package contract

import "time"

type MeasurementStats struct {
	PowerMw   MathStats `json:"powerMw"`
	VoltageMv MathStats `json:"voltageMv"`
	CurrentMa MathStats `json:"currentMa"`
}

type Measurements struct {
	Stats        MeasurementStats `json:"stats"`
	Measurements []*Measurement   `json:"measurements"`
}

func (s *Measurements) Len() int { return len(s.Measurements) }

func (s *Measurements) Swap(i, j int) {
	s.Measurements[i], s.Measurements[j] = s.Measurements[j], s.Measurements[i]
}

func (s *Measurements) Less(i, j int) bool {
	return s.Measurements[i].Timestamp.Before(s.Measurements[j].Timestamp)
}

func (m *Measurements) Compute() {
	var (
		minPowerMw int64 = 0
		maxPowerMw int64 = 0
		sumPowerMw int64 = 0

		minVoltageMv int64 = 0
		maxVoltageMv int64 = 0
		sumVoltageMv int64 = 0

		minCurrentMa int64 = 0
		maxCurrentMa int64 = 0
		sumCurrentMa int64 = 0
	)

	for index, measurement := range m.Measurements {
		if index == 0 { // First iteration, set
			minPowerMw, maxPowerMw = measurement.PowerMw, measurement.PowerMw
			minVoltageMv, maxVoltageMv = measurement.VoltageMv, measurement.VoltageMv
			minCurrentMa, maxCurrentMa = measurement.CurrentMa, measurement.CurrentMa
		} else {
			if measurement.PowerMw < minPowerMw {
				minPowerMw = measurement.PowerMw
			}

			if measurement.VoltageMv < minVoltageMv {
				minVoltageMv = measurement.VoltageMv
			}

			if measurement.CurrentMa < minCurrentMa {
				minCurrentMa = measurement.CurrentMa
			}

			if measurement.PowerMw > maxPowerMw {
				maxPowerMw = measurement.PowerMw
			}

			if measurement.VoltageMv > maxVoltageMv {
				maxVoltageMv = measurement.VoltageMv
			}

			if measurement.CurrentMa > maxCurrentMa {
				maxCurrentMa = measurement.CurrentMa
			}
		}

		sumVoltageMv += measurement.VoltageMv
		sumPowerMw += measurement.PowerMw
		sumCurrentMa += measurement.CurrentMa
	}

	n := int64(len(m.Measurements))

	m.Stats = MeasurementStats{
		PowerMw: MathStats{
			Minimum: minPowerMw,
			Maximum: maxPowerMw,
			Mean:    float64(sumPowerMw / n),
		},
		VoltageMv: MathStats{
			Minimum: minVoltageMv,
			Maximum: maxVoltageMv,
			Mean:    float64(sumVoltageMv / n),
		},
		CurrentMa: MathStats{
			Minimum: minCurrentMa,
			Maximum: maxCurrentMa,
			Mean:    float64(sumCurrentMa / n),
		},
	}
}

type Measurement struct {
	Timestamp time.Time `json:"timestamp"`
	VoltageMv int64     `json:"voltageMv"`
	CurrentMa int64     `json:"currentMa"`
	PowerMw   int64     `json:"powerMw"`
}
