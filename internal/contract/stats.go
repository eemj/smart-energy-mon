package contract

type MathStats struct {
	Mean    float64 `json:"mean"`
	Minimum int64   `json:"minimum"`
	Maximum int64   `json:"maximum"`
}

type ExtendedMathStats struct {
	*MathStats
	StandardDeviation float64 `json:"standardDeviation"`
}
