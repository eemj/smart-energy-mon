package contract

import "time"

type MetricStats struct {
	PowerMw ExtendedMathStats `json:"powerMw"`
}

type Metric struct {
	Device        *Device       `json:"device"`
	Duration      time.Duration `json:"duration"`
	HumanDuration string        `json:"humanDuration"`
	RangeFrom     time.Time     `json:"rangeFrom"`
	RangeTo       time.Time     `json:"rangeTo"`
	Stats         *MetricStats  `json:"stats"`
	PreviousStats *MetricStats  `json:"previousStats"`
}
