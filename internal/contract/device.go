package contract

type Device struct {
	ID         int    `json:"id"`
	MACAddress string `json:"macAddress"`
	Name       string `json:"name"`
}
