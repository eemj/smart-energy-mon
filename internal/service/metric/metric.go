package metric

import (
	"context"
	"math"

	"gitlab.com/eemj/smart-energy-mon/ent"
	"gitlab.com/eemj/smart-energy-mon/internal/contract"
	"gitlab.com/eemj/smart-energy-mon/internal/mapper"
	"gitlab.com/eemj/smart-energy-mon/internal/repository"
	"gitlab.com/eemj/smart-energy-mon/internal/repository/measurement"
)

type metricService struct {
	repo repository.Repository
}

type MetricService interface {
	Get(ctx context.Context, filter GetMetricFilter) (metric *contract.Metric, err error)
}

func NewMetric(
	repo repository.Repository,
) MetricService {
	return &metricService{repo: repo}
}

func (l *metricService) CalculateStats(measurements []*ent.Measurement) (stats *contract.MetricStats) {
	var (
		minPowerMw int64
		maxPowerMw int64
		sumPowerMw int64

		meanPowerMw float64
		sdPowerMw   float64
	)

	n := len(measurements)

	if n > 0 {
		// Calculate the min/max, whilst adding the sum of the power
		for index, measurement := range measurements {
			if index == 0 { // First iteration, set
				minPowerMw = measurement.PowerMw
				maxPowerMw = measurement.PowerMw
			} else {
				if measurement.PowerMw < minPowerMw {
					minPowerMw = measurement.PowerMw
				}

				if measurement.PowerMw > maxPowerMw {
					maxPowerMw = measurement.PowerMw
				}
			}

			sumPowerMw += measurement.PowerMw
		}

		// Using the sum and the number of items, calculate the mean
		meanPowerMw = float64(sumPowerMw / int64(n))

		// Calculate the standard deviation.
		for _, measurement := range measurements {
			sdPowerMw += math.Pow((float64(measurement.PowerMw) - meanPowerMw), 2)
		}

		// Square root the numbers we just **
		sdPowerMw = math.Sqrt(sdPowerMw / float64(n))
	}

	return &contract.MetricStats{PowerMw: contract.ExtendedMathStats{
		MathStats: &contract.MathStats{
			Mean:    meanPowerMw,
			Maximum: maxPowerMw,
			Minimum: minPowerMw,
		},
		StandardDeviation: sdPowerMw,
	}}
}

func (l *metricService) Get(ctx context.Context, filter GetMetricFilter) (metric *contract.Metric, err error) {
	if err := filter.Validate(); err != nil {
		return nil, err
	}

	device, err := l.repo.Device().First(ctx, &ent.Device{ID: filter.DeviceID})

	if err != nil {
		return nil, err
	}

	// Calculate the duration given the filter given from the REST api.
	duration := filter.RangeTo.Sub(filter.RangeFrom)

	measurements, err := l.repo.Measurement().Find(ctx, measurement.MeasurementFilter{
		ID:   filter.DeviceID,
		From: &filter.RangeFrom,
		To:   &filter.RangeTo,
	})

	if err != nil {
		return nil, err
	}

	// Subtract the from the filter, to get the previous generated metrics.
	previousRangeFrom := filter.RangeFrom.Add(-duration)
	previousRangeTo := filter.RangeTo.Add(-duration)

	previousMeasurements, err := l.repo.Measurement().Find(ctx, measurement.MeasurementFilter{
		ID:   filter.DeviceID,
		From: &previousRangeFrom,
		To:   &previousRangeTo,
	})

	if err != nil {
		return nil, err
	}

	return &contract.Metric{
		Device:        mapper.MapDeviceEntityToContract(device),
		Duration:      duration,
		HumanDuration: duration.String(),
		RangeFrom:     filter.RangeFrom,
		RangeTo:       filter.RangeTo,
		Stats:         l.CalculateStats(measurements),
		PreviousStats: l.CalculateStats(previousMeasurements),
	}, nil
}
