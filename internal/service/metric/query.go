package metric

import (
	"errors"
	"fmt"
	"net/http"
	"time"
)

const (
	RangeDuration = time.Hour
)

type GetMetricFilter struct {
	DeviceID  int       `json:"deviceID"`
	RangeFrom time.Time `json:"rangeFrom"`
	RangeTo   time.Time `json:"rangeTo"`
}

func (g GetMetricFilter) Validate() (err error) {
	if g.RangeFrom.IsZero() {
		return errors.New("expected `rangeFrom` query string")
	}

	if g.RangeTo.IsZero() {
		return errors.New("expected `rangeTo` query string")
	}

	if g.RangeTo.Before(g.RangeFrom) {
		return errors.New("`rangeTo` must be after `rangeFrom`")
	}

	if g.RangeTo.Sub(g.RangeFrom) < RangeDuration {
		return fmt.Errorf("`rangeTo` subtracted by `rangeFrom` must be atleast %s", RangeDuration)
	}

	return nil
}

func (g GetMetricFilter) Bind(r *http.Request) {
}
