package shared

import (
	"context"

	"gitlab.com/eemj/smart-energy-mon/ent"
)

type FirstOrCreator[V any] interface {
	First(ctx context.Context, query V) (result V, err error)
	Create(ctx context.Context, query V) (result V, err error)
}

func FirstOrCreate[V any, R FirstOrCreator[V]](
	ctx context.Context,
	repo R,
	v V,
) (result V, err error) {
	result, err = repo.First(ctx, v)

	if (err != nil && ent.IsNotFound(err)) || any(result) == nil {
		return repo.Create(ctx, v)
	}

	return
}
