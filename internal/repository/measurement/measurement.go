package measurement

import (
	"context"

	"gitlab.com/eemj/smart-energy-mon/ent"
	"gitlab.com/eemj/smart-energy-mon/ent/device"
	"gitlab.com/eemj/smart-energy-mon/ent/measurement"
	"gitlab.com/eemj/smart-energy-mon/ent/predicate"
	"gitlab.com/eemj/smart-energy-mon/internal/repository/shared"
)

type MeasurementRepository interface {
	Find(ctx context.Context, filter MeasurementFilter) (results []*ent.Measurement, err error)
	First(ctx context.Context, query *ent.Measurement) (result *ent.Measurement, err error)
	Create(ctx context.Context, query *ent.Measurement) (result *ent.Measurement, err error)
	FirstOrCreate(ctx context.Context, query *ent.Measurement) (result *ent.Measurement, err error)
	Remove(ctx context.Context, query *ent.Measurement) (err error)
	Predicate(query *ent.Measurement) []predicate.Measurement
	Creators(ctx context.Context, queries []*ent.Measurement) []*ent.MeasurementCreate
}

type repository struct {
	client *ent.Client
}

// Find implements MeasurementRepository
func (r *repository) Find(ctx context.Context, filter MeasurementFilter) (result []*ent.Measurement, err error) {
	return r.client.Measurement.Query().
		Where(filter.Predicates()...).
		Order(ent.Desc(measurement.FieldTimestamp)).
		All(ctx)
}

// Create implements MeasurementRepository
func (r *repository) Create(ctx context.Context, query *ent.Measurement) (result *ent.Measurement, err error) {
	if creators := r.Creators(ctx, []*ent.Measurement{query}); len(creators) > 0 {
		return creators[0].Save(ctx)
	}

	return nil, nil
}

// Creators implements MeasurementRepository
func (r *repository) Creators(ctx context.Context, queries []*ent.Measurement) []*ent.MeasurementCreate {
	measurements := make([]*ent.MeasurementCreate, len(queries))

	for index, query := range queries {
		creator := r.client.Measurement.Create()

		if !query.Timestamp.IsZero() {
			creator = creator.SetTimestamp(query.Timestamp)
		}

		creator = creator.SetVoltageMv(query.VoltageMv).
			SetCurrentMa(query.CurrentMa).
			SetPowerMw(query.PowerMw)

		if query.Edges.Device != nil && query.Edges.Device.ID > 0 {
			creator = creator.SetDeviceID(query.Edges.Device.ID)
		}

		measurements[index] = creator
	}

	return measurements
}

// First implements MeasurementRepository
func (r *repository) First(ctx context.Context, query *ent.Measurement) (result *ent.Measurement, err error) {
	return r.client.Measurement.
		Query().
		Where(r.Predicate(query)...).
		First(ctx)
}

// FirstOrCreate implements MeasurementRepository
func (r *repository) FirstOrCreate(ctx context.Context, query *ent.Measurement) (result *ent.Measurement, err error) {
	return shared.FirstOrCreate(ctx, r, query)
}

// Predicate implements MeasurementRepository
func (*repository) Predicate(query *ent.Measurement) []predicate.Measurement {
	predicates := make([]predicate.Measurement, 0)

	if !query.Timestamp.IsZero() {
		predicates = append(predicates, measurement.TimestampEQ(query.Timestamp))
	}

	predicates = append(
		predicates,
		measurement.VoltageMv(query.VoltageMv),
		measurement.CurrentMaEQ(query.CurrentMa),
		measurement.PowerMwEQ(query.PowerMw),
	)

	if query.Edges.Device != nil {
		predicates = append(
			predicates,
			measurement.HasDeviceWith(device.IDEQ(query.Edges.Device.ID)),
		)
	}

	return predicates
}

// Remove implements MeasurementRepository
func (r *repository) Remove(ctx context.Context, query *ent.Measurement) (err error) {
	return r.client.Measurement.DeleteOne(query).Exec(ctx)
}

func New(client *ent.Client) MeasurementRepository {
	return &repository{client: client}
}
