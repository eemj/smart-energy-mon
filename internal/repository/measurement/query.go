package measurement

import (
	"time"

	"gitlab.com/eemj/smart-energy-mon/ent/device"
	"gitlab.com/eemj/smart-energy-mon/ent/measurement"
	"gitlab.com/eemj/smart-energy-mon/ent/predicate"
)

type MeasurementFilter struct {
	ID   int        `json:"id"`
	From *time.Time `json:"from,omitempty"`
	To   *time.Time `json:"to,omitempty"`
}

func (f MeasurementFilter) Predicates() []predicate.Measurement {
	ps := make([]predicate.Measurement, 0)

	if f.ID > 0 {
		ps = append(ps, measurement.HasDeviceWith(device.IDEQ(f.ID)))
	}

	if f.From != nil && !f.From.IsZero() {
		ps = append(ps, measurement.TimestampGTE(*f.From))
	}

	if f.To != nil && !f.To.IsZero() {
		ps = append(ps, measurement.TimestampLTE(*f.To))
	}

	return ps
}
