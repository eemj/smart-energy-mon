package device

import (
	"context"
	"errors"
	"strings"

	"gitlab.com/eemj/smart-energy-mon/ent"
	"gitlab.com/eemj/smart-energy-mon/ent/device"
	"gitlab.com/eemj/smart-energy-mon/ent/predicate"
	"gitlab.com/eemj/smart-energy-mon/internal/repository/shared"
)

type DeviceRepository interface {
	All(ctx context.Context) (results []*ent.Device, err error)
	First(ctx context.Context, query *ent.Device) (result *ent.Device, err error)
	Create(ctx context.Context, query *ent.Device) (result *ent.Device, err error)
	FirstOrCreate(ctx context.Context, query *ent.Device) (result *ent.Device, err error)
	Remove(ctx context.Context, query *ent.Device) (err error)
	Predicate(query *ent.Device) []predicate.Device
	Creators(ctx context.Context, queries []*ent.Device) []*ent.DeviceCreate
}

type repository struct {
	client *ent.Client
}

// All implements DeviceRepository
func (r *repository) All(ctx context.Context) (results []*ent.Device, err error) {
	return r.client.Device.Query().
		Order(ent.Desc(device.FieldMACAddress)).
		All(ctx)
}

// Creators implements DeviceRepository
func (r *repository) Creators(ctx context.Context, queries []*ent.Device) []*ent.DeviceCreate {
	creations := make([]*ent.DeviceCreate, len(queries))

	for index, query := range queries {
		creator := r.client.Device.Create()

		if len(strings.TrimSpace(query.MACAddress)) > 0 {
			creator = creator.SetMACAddress(query.MACAddress)
		}

		if len(strings.TrimSpace(query.Name)) > 0 {
			creator = creator.SetName(query.Name)
		}

		creations[index] = creator
	}

	return creations
}

// Create implements DeviceRepository
func (r *repository) Create(ctx context.Context, query *ent.Device) (result *ent.Device, err error) {
	if creators := r.Creators(ctx, []*ent.Device{query}); len(creators) > 0 {
		return creators[0].Save(ctx)
	}

	return nil, errors.New("wtf")
}

// First implements DeviceRepository
func (r *repository) First(ctx context.Context, query *ent.Device) (result *ent.Device, err error) {
	return r.client.Device.
		Query().
		Where(r.Predicate(query)...).
		First(ctx)
}

// FirstOrCreate implements DeviceRepository
func (r *repository) FirstOrCreate(ctx context.Context, query *ent.Device) (result *ent.Device, err error) {
	return shared.FirstOrCreate(ctx, r, query)
}

// Predicate implements DeviceRepository
func (r *repository) Predicate(query *ent.Device) []predicate.Device {
	predicates := make([]predicate.Device, 0)

	if query.ID > 0 {
		predicates = append(predicates, predicate.Device(device.IDEQ(query.ID)))
	}

	if len(strings.TrimSpace(query.MACAddress)) > 0 {
		predicates = append(predicates, predicate.Device(device.MACAddressEQ(query.MACAddress)))
	}

	return predicates
}

// Remove implements DeviceRepository
func (r *repository) Remove(ctx context.Context, query *ent.Device) (err error) {
	return r.client.Device.DeleteOne(query).Exec(ctx)
}

func New(client *ent.Client) DeviceRepository {
	return &repository{client}
}
