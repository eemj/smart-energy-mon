package repository

import (
	"gitlab.com/eemj/smart-energy-mon/ent"
	"gitlab.com/eemj/smart-energy-mon/internal/repository/device"
	"gitlab.com/eemj/smart-energy-mon/internal/repository/measurement"
)

type repository struct {
	client *ent.Client

	device      device.DeviceRepository
	measurement measurement.MeasurementRepository
}

// Close implements Repository
func (r *repository) Close() (err error) { return r.client.Close() }

// Device implements Repository
func (r *repository) Device() device.DeviceRepository { return r.device }

// Measurement implements Repository
func (r *repository) Measurement() measurement.MeasurementRepository { return r.measurement }

type Repository interface {
	Close() error

	Device() device.DeviceRepository
	Measurement() measurement.MeasurementRepository
}

func New(client *ent.Client) Repository {
	return &repository{
		client: client,

		device:      device.New(client),
		measurement: measurement.New(client),
	}
}
