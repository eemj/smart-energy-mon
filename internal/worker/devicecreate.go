package worker

import (
	"encoding/base64"
	"encoding/hex"
	"fmt"

	"github.com/google/uuid"
	"gitlab.com/eemj/hs110/client"
	client104 "gitlab.com/eemj/hs110/client/v1.0.4"
	client110 "gitlab.com/eemj/hs110/client/v1.1.0"
	"gitlab.com/eemj/hs110/version"
	"gitlab.com/eemj/smart-energy-mon/internal/config"
	"go.uber.org/zap"
)

type Client struct {
	ID      uuid.UUID              `json:"id"`
	Address string                 `json:"address"`
	Client  client.ClientInterface `json:"-"`
}

type ClientError struct {
	Addr string `json:"deviceAddr"`
	Err  error  `json:"err,omitempty"`
}

func (ce *ClientError) Error() string {
	return fmt.Sprintf("%s: %v", ce.Addr, ce.Err)
}

func createDevices(devices []config.Device) (clients []*Client, err error) {
	for _, device := range devices {
		v, err := version.DetermineVersion(device.Address)

		if err != nil {
			return nil, err
		}

		id := uuid.New()
		c := (client.ClientInterface)(nil)

		zap.L().Info(
			"version",
			zap.String("version", string(v)),
			zap.String("addr", device.Address),
			zap.Stringer("id", id),
		)

		switch v {
		case version.Version104:
			c = client104.NewClient(&client104.ClientOptions{
				Host: device.Address,
			})
		case version.Version110:
			c, err = client110.NewClient(&client110.ClientOptions{
				Host:     device.Address,
				Username: *device.Username,
				Password: *device.Password,
			})
		}

		if err != nil {
			return nil, err
		}

		clients = append(clients, &Client{
			ID:     id,
			Client: c,
			Address: hex.EncodeToString([]byte(
				base64.RawStdEncoding.EncodeToString([]byte(device.Address)),
			)),
		})
	}

	return
}
