package worker

import (
	"context"
	"strings"
	"time"

	"gitlab.com/eemj/hs110/api"
	"gitlab.com/eemj/hs110/client"
	"gitlab.com/eemj/smart-energy-mon/ent"
	"gitlab.com/eemj/smart-energy-mon/internal/config"
	"gitlab.com/eemj/smart-energy-mon/internal/repository"
)

type Worker interface {
	Run(ctx context.Context) (err error)
}

type worker struct {
	errc    chan error
	clients []*Client
	repo    repository.Repository
}

func (w *worker) retrieveDevice(
	ctx context.Context,
	client client.ClientInterface,
) (*ent.Device, error) {
	sysInfo, err := api.SysInfo(client)

	if err != nil {
		return nil, err
	}

	macAddress := strings.TrimSpace(strings.ToUpper(sysInfo.System.GetSysInfo.MAC))

	query := &ent.Device{
		MACAddress: macAddress,
		Name:       sysInfo.System.GetSysInfo.Alias,
	}

	return w.repo.Device().FirstOrCreate(ctx, query)
}

// Run implements Worker
func (w *worker) Run(ctx context.Context) (err error) {
	return

	for _, client := range w.clients {
		go func(client *Client) {
			device, err := w.retrieveDevice(ctx, client.Client)

			if err != nil {
				w.errc <- &ClientError{
					Addr: client.Client.Addr(),
					Err:  err,
				}
				return
			}

			ticker := time.NewTicker((1 * time.Second))
			defer ticker.Stop()

			for {
				select {
				case <-ctx.Done():
					return
				case tick := <-ticker.C:
					emeter, _ := api.Emeter(client.Client, api.EmeterRequest{
						Emeter: &api.GetRealtime{GetRealtime: struct{}{}},
					})

					voltageMv, currentMa, powerMw := int64(0), int64(0), int64(0)

					if emeter != nil {
						voltageMv = emeter.Emeter.GetRealtime.VoltageMV
						currentMa = emeter.Emeter.GetRealtime.CurrentMA
					}

					// Calculate the power based on the current milli amp & current milli voltage divided by 1k
					// otherwise the value would be powerKw
					powerMw = (currentMa * voltageMv) / 1000

					if _, err := w.repo.Measurement().Create(ctx, &ent.Measurement{
						Edges:     ent.MeasurementEdges{Device: device},
						VoltageMv: voltageMv,
						CurrentMa: currentMa,
						PowerMw:   powerMw,
						Timestamp: tick.UTC(),
					}); err != nil {
						w.errc <- &ClientError{
							Addr: client.Client.Addr(),
							Err:  err,
						}
						break
					}
				}
			}
		}(client)
	}

	return
}

func New(
	devices []config.Device,
	repo repository.Repository,
) (Worker, error) {
	clients, err := createDevices(devices)

	if err != nil {
		return nil, err
	}

	return &worker{
		errc:    make(chan error, len(clients)), // Buffer the channel to the amount of clients.
		clients: clients,
		repo:    repo,
	}, nil
}
