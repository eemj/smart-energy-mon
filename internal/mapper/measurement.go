package mapper

import (
	"sort"

	"gitlab.com/eemj/smart-energy-mon/ent"
	"gitlab.com/eemj/smart-energy-mon/internal/contract"
)

func MapMeasurementEntityToContract(measurements ...*ent.Measurement) *contract.Measurements {
	result := &contract.Measurements{Measurements: make([]*contract.Measurement, len(measurements))}

	for index, measurement := range measurements {
		result.Measurements[index] = &contract.Measurement{
			Timestamp: measurement.Timestamp,
			VoltageMv: measurement.VoltageMv,
			CurrentMa: measurement.CurrentMa,
			PowerMw:   measurement.PowerMw,
		}
	}

	sort.Sort(sort.Reverse(result)) // Most recent first
	result.Compute()

	return result
}
