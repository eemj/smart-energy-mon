package mapper

import (
	"gitlab.com/eemj/smart-energy-mon/ent"
	"gitlab.com/eemj/smart-energy-mon/internal/contract"
)

func MapDeviceEntityToContract(device *ent.Device) *contract.Device {
	return &contract.Device{
		ID:         device.ID,
		MACAddress: device.MACAddress,
		Name:       device.Name,
	}
}

func MapDevicesEntityToContract(devices ...*ent.Device) []*contract.Device {
	result := make([]*contract.Device, len(devices))

	for index, device := range devices {
		result[index] = MapDeviceEntityToContract(device)
	}

	return result
}
