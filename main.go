package main

import (
	"context"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"

	"go.uber.org/zap"

	"gitlab.com/eemj/smart-energy-mon/ent"
	"gitlab.com/eemj/smart-energy-mon/internal/config"
	"gitlab.com/eemj/smart-energy-mon/internal/httpsrv"
	"gitlab.com/eemj/smart-energy-mon/internal/repository"
	"gitlab.com/eemj/smart-energy-mon/internal/worker"

	_ "github.com/lib/pq"
	_ "github.com/mattn/go-sqlite3"
)

const (
	databaseDriver = "postgres"
)

var (
	Version = "1.0.0"
)

func init() {
	logger, err := zap.NewProduction()

	if err != nil {
		return
	}

	zap.ReplaceGlobals(logger)
}

func createRepo(ctx context.Context, connectionString string) (repository.Repository, error) {
	db, err := ent.Open(databaseDriver, connectionString)

	if err != nil {
		log.Fatal(err)
	}

	if err := db.Schema.Create(ctx); err != nil {
		return nil, err
	}

	return repository.New(db), nil
}

func main() {
	cfg, err := config.New()

	if err != nil {
		zap.L().Fatal("config", zap.Error(err))
	}

	ctx, cancel := context.WithCancel(context.Background())

	defer cancel()

	repo, err := createRepo(ctx, cfg.Database.ConnectionString())

	if err != nil {
		zap.L().Fatal("repository", zap.Error(err))
	}

	worker, err := worker.New(cfg.Devices, repo)

	if err != nil {
		zap.L().Fatal("worker", zap.Error(err))
	}

	go worker.Run(ctx)

	s := make(chan os.Signal, 1)

	go func() {
		httpSrvHandler := httpsrv.New(repo)

		port := os.Getenv("PORT")

		if port == "" {
			port = "4000"
		}

		L := func() *zap.Logger {
			return zap.L().Named("http")
		}

		L().Info("attempt_listen", zap.String("port", port))

		httpSrv := &http.Server{
			Addr:    (":" + port),
			Handler: httpSrvHandler,
		}

		if err := httpSrv.ListenAndServe(); err != nil {
			L().Fatal("listen", zap.Error(err))
		}
	}()

	signal.Notify(s, syscall.SIGINT, syscall.SIGKILL, syscall.SIGTERM)
	<-s
}
